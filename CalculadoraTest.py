# coding=utf-8

from unittest import TestCase

from Calculadora import Calculadora


class CalculadoraTest(TestCase):
    def test_sumar(self):
        self.assertEquals(Calculadora().sumar(""),0,"Cadena Vacía")

    def test_sumar_unacadena(self):
        self.assertEquals(Calculadora().sumar("1"), 1, "Un numero")
        self.assertEquals(Calculadora().sumar("2"), 2, "Un numero")

    def test_sumar_cadena_dosnumeros(self):
        self.assertEquals(Calculadora().sumar("1,3"), 4, "Dos numeros")

    def test_sumar_cadena_multiplesnumeros(self):
        self.assertEquals(Calculadora().sumar("5,2,4,1"), 12, "Multiple numeros")

    def test_sumar_cadena_multiplesnumeros_distintosseparadores(self):
        self.assertEquals(Calculadora().sumar("5%2&4:1:2&8"), 22, "Multiple numeros distintos separadores")