# coding=utf-8

class Calculadora:

    def sumar(self, cadena):
        if cadena is None or cadena == "":
            return 0
        elif "," in cadena or "%" in cadena or "&" in cadena or "%" in cadena or ":" in cadena:
            res = 0
            for indice in range(len(cadena)):
                if indice % 2 == 0:
                    res += int(cadena[indice])
            return res
        return int(cadena)
